package monitor

import (
	"bbip/manager"
	"bbip/myconfig"
	"bbip/myloger"
	"bufio"
	"os"
	"regexp"
	"strconv"
	"time"
)

// Start monitoring error in logfile based on one rule from config. If detected bad ip, send
// it to queue to blocking. Bei existing logfile begin from last lone. Bei new logfile begin
// from fist line.
// @param rule myconfig.LogfileRule - rule to detect scann in logfile.
// @param ipq chan-< string - queue where send bad ip
// @param loger *myloger.Loger
func StartLogMonitor(rule myconfig.LogfileRule, manq chan<- manager.BadIPMessage, loger *myloger.Loger) {

	rule.Name = myloger.Kawichki(rule.Name)
	loger.Info("Launch logmonitor for " + rule.Name + ". Error patern " +
		myloger.Kawichki(rule.Error) + ". Bad IP patern " + myloger.Kawichki(rule.IP))

	// The flag used to detect "first intervall".  By first intervall only read till end
	// without prozessing readed lines.
	var runfirst bool = true

	// Last state of logfile. Used to detect that file is recreated/rotated.
	var laststat os.FileInfo

	// Compiled regexpression for Error in text-line
	rxerr, _ := regexp.Compile(rule.Error)

	// Compiled regexpression for Error in text-line
	rxexc, _ := regexp.Compile(rule.Exclude)

	// Compiled regexpression for IP-Address in text-line
	rxipa, _ := regexp.Compile(rule.IP)

	// In this loop continuously read from logfile. If new logfile with the same
	// name created, leave the loope to reopen the logfile at begin.
	for {

		lf, err := os.Open(rule.LogPath)
		if err != nil {
			loger.Warn("Failed open logfile " + rule.LogPath)
			time.Sleep(time.Duration(rule.Interval) * time.Second)
			continue
		}

		loger.Info("Open logfile " + rule.LogPath)

		// By open save state of logfile
		laststat, _ = lf.Stat()

		// Loop processing new lines each interval
		for {

			// Detect if logfile recreated based on size, if true close old and reopen again.
			if isNewLogfile(laststat.Size(), rule.LogPath) {
				break
			}

			// The scanner used to read line by line
			scanner := bufio.NewScanner(lf)

			// First time only go to the end of logfile.
			if runfirst {
				runfirst = false
				for scanner.Scan() {
				}
				time.Sleep(time.Duration(rule.Interval) * time.Second)
				continue
			}

			// "Result of Interval". Reset error counter. Format "ip" -> count
			var reinterval = make(map[string]int)

			// Get new lines from logfile and process each. To save memory I process each line separate without
			// save it in array.
			for scanner.Scan() {

				line := scanner.Text() // Last new line charachter is dropped!

				// Ignore if line contains excluded pattern
				refindex := rxexc.FindString(line)
				if refindex != "" {
					loger.Debug("Ignore line containing excluded pattern " + myloger.Kawichki(rule.Exclude))
					continue
				}

				// Seach for error patern. If error found try extract ip. If ip found,
				// count up errors per ip.
				refinder := rxerr.FindString(line)

				if refinder != "" {

					// Result is array. First element is always string that mach expression.
					// If regexpression to get ip contains group defined by (), second element
					// ist string in the group. The regexpression to get ip may be defined with
					// or without group, because of it I consider two cases.
					refindip := rxipa.FindStringSubmatch(line)

					if len(refindip) == 1 {
						reinterval[refindip[0]]++
						loger.Debug("Detect bad ip " + refindip[0] + ". Error pattern " + myloger.Kawichki(refinder))
					} else if len(refindip) > 1 {
						reinterval[refindip[1]]++
						loger.Debug("Detect bad ip " + refindip[1] + ". Error pattern " + myloger.Kawichki(refinder))
					}
				}
			}

			// If all new lines are processed, process result of this interval. If result
			// isn't empty send and counter per ip is bigger as configured max-times, send
			// bad ip in queue to block.
			if len(reinterval) > 0 {
				for ip, counter := range reinterval {
					if reinterval[ip] >= rule.TimesMax {
						manq <- manager.BadIPMessage{IP: ip, Rule: rule.Name}
						loger.Debug("Put bad ip " + ip + " in queue. Counter per interval " + strconv.Itoa(counter))
					}
				}
			}

			// Update stat of logfile
			laststat, _ = lf.Stat()

			//loger.Info("End intervall")
			time.Sleep(time.Duration(rule.Interval) * time.Second)
		}

		// Close logfile
		loger.Info("Close logfile " + rule.LogPath)
		lf.Close()

		// Prevent reopen logfile frequently
		time.Sleep(time.Duration(rule.Interval) * time.Second)
	}

}

// Detect if logfile recreated. May be two cases.
// 1 - the same logfile (the same inode) resetet to 0 and write again.
// 2 - new logfile created with the same name but another inode.
// In both cases most likely that "new logfile" is smaller as "old".
// @param lastsize int64 - size of old logfile
// @param logpath string - path to logfile
// @return boolean - true if logfile recreated or read properiest failed
func isNewLogfile(lastsize int64, logpath string) bool {

	newlf, err := os.Open(logpath)

	if err != nil {
		return true
	}

	newstat, _ := newlf.Stat()

	// Return true if new size is smaller als last size, else return false.
	return newstat.Size() < lastsize
}
