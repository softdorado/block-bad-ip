package monitor

import (
	"bbip/manager"
	"bbip/myconfig"
	"bbip/myloger"
	"net"
	"strings"
)

// Start Honeypot-Rule to detect network-scanner
func StartHoneypot(rule myconfig.HoneypotRule, ipq chan<- manager.BadIPMessage, loger *myloger.Loger) {
	rule.Name = myloger.Kawichki(rule.Name)
	loger.Info("Launch honeypot " + rule.Name + " bind to" + rule.Bind)

	// Listen for incoming connections.
	l, err := net.Listen("tcp", rule.Bind)
	if err != nil {
		loger.Fatal(rule.Name + " Failed start honeypot." + err.Error())
	}

	// Close the listener when the application closes.
	defer l.Close()

	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			loger.Fatal(rule.Name + " Failed accept connection. " + err.Error())
		}

		// Get IP from connected client.
		remote := strings.Split(conn.RemoteAddr().String(), ":")
		loger.Info(rule.Name + " Connection from IP " + remote[0] + " Port " + remote[1])
		ipq <- manager.BadIPMessage{IP: remote[0], Rule: rule.Name}
		conn.Close()
	}
}
