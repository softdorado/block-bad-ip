package myconfig

// The names of members in Strucktur should be in capital letters!
// Else convert JSON to struct don't work!!!!

import (
	"bbip/myloger"
	"bufio"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
)

// Main stuctur for application configuration
type Config struct {
	Common     CommonParameter // Not in XML!
	Honeypot   []HoneypotRule  `xml:"honeypot>honeypotrule"`
	LogMonitor []LogfileRule   `xml:"logmonitor>logfilerule"`
}

// Common parameters of application
type CommonParameter struct {

	// Path to this configuration files based on Config-Direcory + Filename
	// Config-Direcotry is startparameter -c. Default "." oder "etc/bbip"
	// Depend where programm find file config.xml

	// Reg. expression for trusted IP addresses. Not from XML. Read from file
	// and build single regxpression contaiing lines form file line1|line2|etc...
	// Path to file = Config-Direcory + /trusted-ip.txt.
	TrustedIP string

	// Reg. expression for trusted DNS Hostname. Not from XML. Read from file
	// and build single regxpression containg lines from file line1|line2|ect ...
	// Path to file = Config-Direcory + /trusted-dns.txt.
	TrustedDNS string

	// Path to CSV file were programm save data. Not from XML.
	// Path to file = Config-Direcory + /ipdb.txt.
	DataFile string
}

// Config rule for TCP honeypot
type HoneypotRule struct {
	// Logical name of rule
	Name string `xml:"name"`

	// IP-Address and TCP-Port to bind. Example for all interfases "0.0.0.0:3307"
	Bind string `xml:"bind"`

	// Flag enable/disable rule. Default true
	Enable bool `xml:"enable"`
}

// Config rule for logfile based intrusion detection
type LogfileRule struct {

	// Logical name of rule
	Name string `xml:"name"`

	// Path to logfile
	LogPath string `xml:"logpath"`

	// Search patern for error in onle line of logfile.
	Error string `xml:"error"`

	// Ignore line if line contains this pattern
	Exclude string `xml:"exclude"`

	// Seach patern for IP. Used only if error detected. Default "(\d[1,3]\.\d[1,3]\.\d[1,3]\.\d[1,3])"
	IP string `xml:"ip"`

	// Maximal times error accepted.
	TimesMax int `xml:"timesmax"`

	// In which interval (secodnds) check logfile. Default 5.
	Interval int `xml:"interval"`

	// Flag enable/disable rule. Default/"if not defined" false.
	Enable bool `xml:"enable"`
}

const regexipDefault string = `(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})`
const intervalDefault int = 2
const timesmaxDefault int = 1

// Read JSON-file and conver to struct Config. Return pointer to struct Config.
// @param string jsonfilepath - Path to JSON-file
// @param *myloger.Loger - Pointer to logger
// @return - pointer to struct Config
func New(cfgdir string, loger *myloger.Loger) *Config {

	var config Config

	// If config directory not empty look there for config files. If empty try
	// default "." or "/etc/bbip"
	if cfgdir != "" {

		if _, err := os.Stat(cfgdir + "/config.xml"); err != nil {
			loger.Fatal("Config file " + cfgdir + "/config.xml not found")
		}

	} else {

		// Try "." oder "/etc/bbip"
		if _, err := os.Stat("./config.xml"); err == nil {
			cfgdir = "."
		} else if _, err := os.Stat("/etc/bbip/config.xml"); err == nil {
			cfgdir = "/etc/bbip"
		} else {
			loger.Fatal("Config file ./config.xml or /etc/bbip/config.xml not found.")
		}
	}

	// Read entire content of config file and check if error occure
	xmlstring, err := ioutil.ReadFile(cfgdir + "/config.xml")
	if err != nil {
		loger.Fatal("Failed read config file " + myloger.Kawichki(cfgdir+"/config.xml"))
	}

	// Encode XML-string to config struct.
	err = xml.Unmarshal(xmlstring, &config)
	if err != nil {
		loger.Fatal("Failed encode configuration file " + myloger.Kawichki(cfgdir) + " Check with xmllint if file XML conform.")
	}

	// Set path to data file
	config.Common.DataFile = cfgdir + "/ipdb.csv"

	// Create reg exprtession from data in files
	config.Common.TrustedIP = filetoregexp(cfgdir+"/trusted-ip.txt", loger)
	config.Common.TrustedDNS = filetoregexp(cfgdir+"/trusted-dns.txt", loger)

	// If no honeypot rules and no log monitor rules defined it is invalid config. In this case
	// program crash due to "deadlock". "Deadlock" occure because only reading thread for ip-queue
	// is active.
	if config.LogMonitor == nil && config.Honeypot == nil {
		loger.Fatal("Invalid configuration. No rules defined.")
	}

	// If not defined set path to svc datafile. (Develompent on Windows!)
	config.Common.DataFile = cfgdir + "/ipdb.csv"

	// Check parmater in rules for Honeypot.
	for _, rule := range config.Honeypot {

		// Don't check disabled rules
		if !rule.Enable {
			continue
		}

		// If in rule parameter name missing abort execition.
		if rule.Name == "" {
			loger.Fatal("Invalid configuration. One Honeypot rule without parameter \"name\"")
		}

		// If in rule path to logfile not defined abort execution.
		if rule.Bind == "" {
			loger.Fatal("Invalid configuration. In rule " + myloger.Kawichki(rule.Name) + " missing parameter bind")
		}
	}

	// Check parameters in rules for LogMonitor and if need set default values. The variable rule
	// is a copy of rule in object! To check some thik the copy is enought, but to change value
	// use index based access to original!
	for i, rule := range config.LogMonitor {

		// Don't check disabled rules
		if !rule.Enable {
			continue
		}

		// If in rule parameter name missing abort execition.
		if rule.Name == "" {
			loger.Fatal("Invalid configuration. One Logmonitor rule without parameter \"name\"")
		}

		// If in rule path to logfile not defined abort execution.
		if rule.LogPath == "" {
			loger.Fatal("Invalid configuration. In rule " + myloger.Kawichki(rule.Name) + " missing parameter logpath")
		}

		// If  file not exists abort execution
		if _, err = os.Stat(rule.LogPath); err != nil {
			loger.Fatal("Invalid configuration. In rule " + myloger.Kawichki(rule.Name) + " logfile " + rule.LogPath + " not found")
		}

		// If in rule element "error" missing abort execition.
		if rule.Error == "" {
			loger.Fatal("Invalid configuration. In rule " + myloger.Kawichki(rule.Name) + " missing parameter <error>")
		}

		// Set default reg expression to dected IP
		if rule.IP == "" {
			config.LogMonitor[i].IP = regexipDefault
			loger.Warn("Rule " + myloger.Kawichki(rule.Name) + " set parameter <ip> default value " + myloger.Kawichki(rule.IP))
		}

		// Set default timesmax if not defined
		if rule.TimesMax == 0 {
			config.LogMonitor[i].TimesMax = timesmaxDefault
			loger.Warn("Rule " + myloger.Kawichki(rule.Name) + " set parameter <timesmax> default value " + strconv.Itoa(rule.TimesMax))
		}

		// Set default interval time.
		if rule.Interval == 0 {
			config.LogMonitor[i].Interval = intervalDefault
			loger.Warn("Rule " + myloger.Kawichki(rule.Name) + " set parameter <interval> default value " + strconv.Itoa(rule.Interval))
		}

		// Check if patern for regexpression/ is valid
		_, err := regexp.Compile(rule.Error)
		if err != nil {
			loger.Fatal("Rule " + myloger.Kawichki(rule.Name) + " invalid patern in element <error>")
		}

		// Check if patern for regexpression/ is valid
		_, err = regexp.Compile(rule.Exclude)
		if err != nil {
			loger.Fatal("Rule " + myloger.Kawichki(rule.Name) + " invalid patern in element <exclude>")
		}

		// Check if patern for regexpip is valid
		_, err = regexp.Compile(rule.IP)
		if err != nil {
			loger.Fatal("Rule " + myloger.Kawichki(rule.Name) + " invalid patern in parameter <ip>")
		}
	}

	return &config
}

// Join lines in file to one big reg expression. Each pattern separated by "|" from other.
// @param cfgfile string - path to file
// @retrun string
func filetoregexp(pathtofile string, loger *myloger.Loger) string {

	// Empty result
	var rx string = ""

	// If file not exist return empty
	if _, err := os.Stat(pathtofile); err != nil {
		loger.Warn("File " + pathtofile + " not found")
		return rx
	}

	cf, erropen := os.Open(pathtofile)

	if erropen != nil {
		loger.Fatal("Failed open " + pathtofile)
	}

	// Reg expression to detect line with comments
	rxcoment, _ := regexp.Compile(`^#`)

	// Scanner to read all lines o file
	scanner := bufio.NewScanner(cf)

	// Get alle lines from file. Ignore empty and line with # at begin. This lines joint
	// to one big reg expression
	var counter int = 0
	for scanner.Scan() {

		line := scanner.Text()
		line = strings.TrimSpace(line)

		// Ignore coments and empty lines
		if rxcoment.FindString(line) == "" && line != "" {
			rx = rx + "|" + line
		}

		counter++
	}

	// Remove first "|" in string
	rx = strings.TrimPrefix(rx, "|")

	cf.Close()

	if counter > 0 {
		fmt.Println(rx)
	}

	return rx
}
