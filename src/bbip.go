// BSD 3-Clause License Copyright (c) 2021, Andrej Koslov All rights reserved.

package main

import (
	"bbip/manager"
	"bbip/monitor"
	"bbip/myconfig"
	"bbip/myloger"
	"flag"
	"fmt"
	"os"
	"runtime"
	"time"
)

const version string = "1.0.0.1" // Current version
var loger *myloger.Loger         // Object for logger
var config *myconfig.Config      // Global config

func init() {

	// Path to config directory as startparameter. See more in myconfig/myconfig.go
	var cfgdir string
	flag.StringVar(&cfgdir, "c", "", `Path to configuration directory. Default . or /etc/bbip. Programm expected
in the configuration directroy config.xml, trusted-ip.txt, trusted-dns.txt
and save there the database dbip.csv`)

	// Link variable logfile to start parameter. Default "./bbip.log"
	var logfilepath string
	flag.StringVar(&logfilepath, "l", "", `Path to logfile. If not defined output on console.`)

	// Link variable loglevel to start parameter. Default 4 INFO
	var loglevel int
	flag.IntVar(&loglevel, "v", 4, "Loglevel. 5=Devug, 4=Info, 3=Warning, 2=Error, 1=Fatal, 0=NoLog")

	// Output short help
	if len(os.Args) > 1 && (os.Args[1] == "-h" || os.Args[1] == "-?" || os.Args[1] == "?") {
		fmt.Print("Block-Bad-IP is intelligente system to reduce possibility abuse online systeme\n")
		fmt.Print("bbip version " + version + "\n\n")
		flag.PrintDefaults()
		fmt.Print("\nFor start in background use bbipd\n")
		os.Exit(0)
	}

	flag.Parse()

	loger = myloger.New(logfilepath, loglevel)
	config = myconfig.New(cfgdir, loger)
}

func main() {

	loger.Info("")
	loger.Info("Start bad IP blocker. Version " + version + " Running on " + runtime.GOOS)
	loger.Info("")
	loger.Info("Trusted IP " + myloger.Kawichki(config.Common.TrustedIP))
	loger.Info("Trusted DNS " + myloger.Kawichki(config.Common.TrustedDNS))
	loger.Info("Database " + myloger.Kawichki(config.Common.DataFile))

	// Managment queue for detected bad IP. Monitoring threads (Honeypopt and Logmonitor) send
	// detected bad ip to managment thread.
	var manq chan manager.BadIPMessage = make(chan manager.BadIPMessage, 100)

	// Start managment
	manager.Start(manq, config, loger)

	// Start Honeypot for each enabled rule
	for _, rule := range config.Honeypot {
		if rule.Enable {
			go monitor.StartHoneypot(rule, manq, loger)
		}
	}

	// Start Logmonitor for each enabled rule
	for _, rule := range config.LogMonitor {
		if rule.Enable {
			go monitor.StartLogMonitor(rule, manq, loger)
		}
	}

	// Don't exist. May be later output statistics.
	for {
		time.Sleep(time.Hour)
	}
}
