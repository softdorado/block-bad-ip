#! /bin/bash

## Remove BBIP 

echo
echo "Start deinstallation Block-Bad-IP"

## Check if systemctl availabel. need to setup autostart with Systemd.
systemctl --version 1>/dev/null 2>/dev/null

if [ $? -eq 0 ] ; then 
    echo "Disable autostart bbip service"
    systemctl disable bbip
else 
    echo "Systemctl not found. Please disable autostart manuel."
fi

if [ -f /usr/local/bin/bbip ] ; then
    echo "Remove /usr/local/bin/bbip"
    rm /usr/local/bin/bbip
fi

if  [ -f /usr/local/bin/bbipd ] ; then
    echo "Remove /usr/local/bin/bbipd"
    rm /usr/local/bin/bbipd
fi

if [ -d /etc/bbip ] ; then
    echo "Remove examples from /etc/bbip"
    echo "Other configuration files stay availabel"
    rm -f /etc/bbip/*example.*
    rm -f /etc/bbip/bbip.service
fi

if [ -f /usr/share/man/man1/bbip.1.gz ] ; then
    echo "Remove man-pages"
    rm -f /usr/share/man/man1/bbip.1.gz
fi

if [ -d /var/log/bbip ] ; then
    echo "Remove /var/log/bbip"
    rm -rf /var/log/bbip
fi
