package manager

import (
	"bbip/myloger"
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// the structur used to track information about bad ip in csv and memory
type IPRecord struct {
	ipaddr   string // IP address
	firstdt  string // Data time as string. Dirst detected. Format time.RFC3339
	lastdt   string // Data time as string. Last detected. Format time.RFC3339
	lastts   int64  // Unix timestamp. Last detected
	counter  int    // Counter. By 1 block for hour, 2 block for a day, more block for a week
	blocked  bool   // Status of blocking. If rule is active true, else false.
	rule     string // Name of last rule that detect this ip
	hostname string // Hostname that return DNS reverse lookup. Hostname without last dot! If not found "-"
}

// Save database in memory to CSV with delimiter ";"". By error cancel execution.
// @param pathtocsv string - path to CSV file.
// @param loger *myloger.Loger - Logger.
func savedb(requestor string, pathtocsv string, loger *myloger.Loger) {

	cf, erropen := os.Create(pathtocsv)
	if erropen != nil {
		loger.Fatal("Failed to create cvs file " + pathtocsv + " Last error " + erropen.Error())
	}

	writer := bufio.NewWriter(cf)
	var counter int = 0

	// Read-Lock database before save it
	locker.RLock()
	for _, re := range ipdb {

		// from ip record build line for CVS file
		var line string = re.ipaddr + ";" + re.firstdt + ";" + re.lastdt + ";" +
			fmt.Sprint(re.lastts) + ";" + fmt.Sprint(re.counter) + ";" +
			fmt.Sprint(re.blocked) + ";" + re.rule + ";" + re.hostname

		_, errwrite := writer.WriteString(line + "\n")
		if errwrite != nil {
			loger.Error("Failed write line " + myloger.Kawichki(line) + " to csv")
			loger.Fatal("Last error " + errwrite.Error())
		}
		counter++
	}
	locker.RUnlock()

	writer.Flush()
	cf.Close()

	loger.Debug(requestor + " save in " + myloger.Kawichki(pathtocsv) + " " + fmt.Sprint(counter) + " records")
}

// Read from CSV line by line and convert to ip records. Each record add to map.
// This map act as in memory database. If CVS not exists create empty.
// @return map[string]*iprecord - acting as in memory database.
func loaddb(pathtocsv string, loger *myloger.Loger) map[string]*IPRecord {

	// If scv file not exist create empty
	if _, errstat := os.Stat(pathtocsv); errstat != nil {
		ef, errcreate := os.Create(pathtocsv)
		if errcreate != nil {
			loger.Fatal("Failed create new csv file " + pathtocsv)
		}
		ef.Close()
	}

	// Create empty db
	ipdb := make(map[string]*IPRecord)

	cf, erropen := os.Open(pathtocsv)

	if erropen != nil {
		loger.Fatal("Failed open csv file " + pathtocsv)
	}

	var linenum int = 0
	scanner := bufio.NewScanner(cf)

	for scanner.Scan() {

		// Get ome line and just convert to array
		record := strings.Split(scanner.Text(), ";")

		linenum++

		// Convert data from line to fields in ip records. Check in valid data.
		if len(record) != 8 {
			loger.Fatal("Record in database contains invalid amound of fileds. Should be 8. Found " + fmt.Sprint(len(record)) + ". Line " + fmt.Sprint(linenum))
		}

		// IP address
		var ipaddr string = record[0]

		// Data-time first occurence. Format time.RFC3339
		var firstdt string = record[1]
		if firstdt == "" {
			loger.Fatal("Database contains empty first occurence data-time ip " + ipaddr + ". Line " + fmt.Sprint(linenum))
		}

		// Data-time lact occurence. Format time.RFC3339
		var lastdt string = record[2]
		if lastdt == "" {
			loger.Fatal("Database contains empty last occurence data-time ip " + ipaddr + ". Line " + fmt.Sprint(linenum))
		}

		// Timestamp int64
		lastts, errts := strconv.ParseInt(record[3], 10, 64)
		if errts != nil {
			loger.Fatal("Database contains invalid timestamp " + record[3] + " ip " + ipaddr + ". Line " + fmt.Sprint(linenum))
		}

		// Counter int
		counter, errcounter := strconv.Atoi(record[4])
		if errcounter != nil {
			loger.Error("Database contains invalid counter " + record[4] + " ip " + ipaddr + " Set counter 1. Line " + fmt.Sprint(linenum))
			counter = 1
		}

		// Block status
		blocked, errblocked := strconv.ParseBool(record[5])
		if errblocked != nil {
			loger.Error("Database contains invalid blocking status rule ip " + ipaddr + ". Set true. Line " + fmt.Sprint(linenum))
			blocked = true
		}

		// Rule name
		rule := record[6]
		if rule == "" {
			loger.Error("Database contains empty rule ip " + ipaddr + " Set \"Unknown rule\". Line " + fmt.Sprint(linenum))
			rule = "Unknow rule"
		}

		// DNS Hostname. If in CSV missing set default.
		var hostname string = "-"
		if len(record) > 7 {
			hostname = record[7]
		}

		// New ip record add to map
		ipdb[ipaddr] = &IPRecord{ipaddr: ipaddr, firstdt: firstdt, lastdt: lastdt, lastts: lastts, counter: counter, blocked: blocked, rule: rule, hostname: hostname}
	}

	cf.Close()

	return ipdb
}
