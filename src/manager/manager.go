package manager

// First time block for an hour, secondt time block for a day, third and more block for a week.
// If last occurence older as two weeks remove from database. At begin block all ip with
// time-stamp that are not older as one week.

import (
	"bbip/myconfig"
	"bbip/myloger"
	"fmt"
	"net"
	"os/exec"
	"regexp"
	"runtime"
	"sync"
	"time"
)

// Struct used as message between monitoring threads and manager.
type BadIPMessage struct {
	IP   string // IP addrsss
	Rule string // Name of rule where bad ip detected
}

// Path to iptables eecutable
// Block -A INPUT -s %IP% -j REJECT",
// Reset -D INPUT -s %IP% -j REJECT",
const iptables string = "/sbin/iptables"

// Block-time
const (
	BLOCK_HOUR int64 = 3600
	BLOCK_DAY  int64 = 3600 * 7
	BLOCK_WEEK int64 = 3600 * 24 * 7
)

// Map as database containing information about all blocked ip. Each element is pointer to struct.
var ipdb map[string]*IPRecord

// Used for exclisive access to db bei save and changes
var locker sync.RWMutex

// At beginn - delete all rules in INPUT, load data from svc, create blockung rules for ip from db
// that should be blocked.
func Start(manq chan BadIPMessage, config *myconfig.Config, loger *myloger.Loger) {

	// Load current database
	ipdb = loaddb(config.Common.DataFile, loger)
	loger.Info("Load database with " + fmt.Sprint(len(ipdb)) + " records")

	if runtime.GOOS != "windows" {

		// Delete all old firewall-rules in INPUT channel. Then add blocking rules only
		// where last occurrence (time-stamp) not older as one week.

		// Remove all rules
		cmd := exec.Command(iptables, "-F", "INPUT")
		if err := cmd.Run(); err != nil {
			loger.Fatal("Failed delete old rules in channel INPUT " + err.Error())
		}

		// Add valid rules and set blocking status. Lock don't need because only this
		// thread accesss map-db.
		now := time.Now()
		for _, record := range ipdb {

			if (record.lastts + getblocktime(record.counter)) > now.Unix() {
				blockip(record.ipaddr)
				record.blocked = true
				loger.Info("Initial block ip " + record.ipaddr)
			} else {
				record.blocked = false
				loger.Debug("Initial ignore ip " + record.ipaddr)
			}
		}
	}

	// Start blocking thread
	go blocker(manq, config, loger)

	// Start reseting thread
	go reseter(loger, config.Common.DataFile)
}

// Take ip from management queue, block it and update data in database
func blocker(manq chan BadIPMessage, config *myconfig.Config, loger *myloger.Loger) {

	// Name of thread. Used to identify log messages.
	const THNAME string = "Blocker"

	// Compiled regexpression for trusted ip addresses
	rxtipa, _ := regexp.Compile(config.Common.TrustedIP)

	// Compiled regexpression for trusted DNS hostname
	rxtdns, _ := regexp.Compile(config.Common.TrustedDNS)

	// Start processing bad ip from monitoring threads
	for {
		msg := <-manq
		loger.Info("Blocker got bad ip " + myloger.Kawichki(msg.IP) + " rule " + msg.Rule)

		// Reverse loockup DNS
		hostname := gethostname(msg.IP)

		// Check if dns match trusded pattern, if yes skip line
		if config.Common.TrustedDNS != "" && hostname != "-" {

			trustdns := rxtdns.FindString(hostname)
			if trustdns != "" {
				loger.Info(THNAME + " skip trusted hostname " + trustdns)
				continue
			}
		}

		// Check if ip match trusted pattern, if yes skip line
		if config.Common.TrustedIP != "" {

			trustip := rxtipa.FindString(msg.IP)
			if trustip != "" {
				loger.Info(THNAME + " skip trusted IP " + msg.IP)
				continue
			}
		}

		// current time
		dt := time.Now()

		// Check if ip is already known. New ip save in db and block it. If ip already known
		// updaten properties and block. Reset-thread will remove blocking rule if block-time end.
		// Variable oldrecord point to real struct in map. Without pointer map return copy of struct!
		oldrecord := ipdb[msg.IP]

		if oldrecord == nil {

			// This ip is inknown.

			// Create new record and add to db
			newrecord := IPRecord{
				ipaddr:  msg.IP,
				firstdt: dt.Format(time.RFC3339),
				lastdt:  dt.Format(time.RFC3339),
				lastts:  dt.Unix(), counter: 1,
				blocked:  true,
				rule:     msg.Rule,
				hostname: hostname,
			}
			locker.Lock()
			ipdb[msg.IP] = &newrecord
			locker.Unlock()

			// Save db with new record
			savedb(THNAME, config.Common.DataFile, loger)

			// Block new bad ip
			loger.Info("Block new bad IP " + msg.IP + " " + hostname)
			err := blockip(msg.IP)
			if err != nil {
				loger.Fatal("Error by executing block command for " + msg.IP + " " + err.Error())
			}

		} else {

			// Bad ip is already know. Update oldrecord in db and block again.

			// Check if ip is already blocked. It may happen if ip detected in more as one rule
			// quasi simultaneously. The command to block second time throw no error, but reseter
			// remove only one rule and ip forever stay blocked!
			if oldrecord.blocked {
				loger.Info("The ip " + msg.IP + " is already bocked")
				continue
			}

			locker.Lock()
			oldrecord.lastts = dt.Unix()
			oldrecord.lastdt = dt.Format(time.RFC3339)
			oldrecord.counter++
			oldrecord.blocked = true
			oldrecord.rule = msg.Rule
			oldrecord.hostname = hostname
			locker.Unlock()

			savedb(THNAME, config.Common.DataFile, loger)

			loger.Info("Block known bad IP " + msg.IP + " " + hostname + " counter " + fmt.Sprint(oldrecord.counter))

			err := blockip(msg.IP)
			if err != nil {
				loger.Fatal("Error by executing block command for " + msg.IP + " " + err.Error())
			}
		}
	}
}

// Each minute reset ip due end of blocktime and if no occurence last two weeks
// remove from db.
func reseter(loger *myloger.Loger, pathtocsv string) {

	// Name of thread. Used to idetify log messages.
	const THNAME string = "Reseter"

	for {

		time.Sleep(time.Second * 60)

		now := time.Now()
		var limit2w int64 = 3600 * 24 * 14 // Limit oldest records two weeks
		var toremove []string

		// Loop over all records.
		for _, record := range ipdb {

			// Reset ip if blocktime is ended and blocking status is true. Don't check result,
			// because if for ip no rules, iptables return error!
			blocktime := getblocktime(record.counter)
			if (record.lastts+blocktime) < now.Unix() && record.blocked {
				locker.Lock()
				resetip(record.ipaddr)
				record.blocked = false
				locker.Unlock()
				loger.Info("Reset ip " + record.ipaddr + " " + record.hostname)
			}

			// Select ip which since two weeks not as bad ip detected. This
			// ip save in array and after this loop remove from map.
			if record.lastts+limit2w < now.Unix() {
				toremove = append(toremove, record.ipaddr)
			}
		}

		// Remove old record from db. Do it with lock/unlock
		if len(toremove) > 0 {

			locker.Lock()
			for _, value := range toremove {
				record := ipdb[value]
				loger.Info(fmt.Sprintf("Remove ip %s %s from database", value, record.hostname))
				delete(ipdb, value)
			}
			locker.Unlock()

			savedb(THNAME, pathtocsv, loger)
		}
	}
}

// Return blocking-time based on counter. Counter is occurence how much ip
// detected as "bad" ip.
func getblocktime(counter int) int64 {

	if counter == 1 {
		return BLOCK_HOUR
	}

	if counter == 2 {
		return BLOCK_DAY
	}

	return BLOCK_WEEK
}

// Execute external command to delete rule blocking one ip. If no rule for ip
// iptables return error!
// @param ip string - ip address to reset
// @return error - if successfull true, esle false
func resetip(ip string) error {

	if runtime.GOOS == "windows" {
		return nil
	}

	cmd := exec.Command(iptables, "-D", "INPUT", "-s", ip, "-j", "REJECT")
	err := cmd.Run()
	if err != nil {
		// Return orignal error. For new error errors.New("empty name")
		return err
	}

	return nil
}

// Execute external command to create rule blockng one ip.
// @param ip string - ip address to block
// @return error - if successfull true, esle false
func blockip(ip string) error {

	if runtime.GOOS == "windows" {
		return nil
	}

	cmd := exec.Command(iptables, "-A", "INPUT", "-s", ip, "-j", "REJECT")
	err := cmd.Run()
	if err != nil {
		// Return orignal error. For new error errors.New("empty name")
		return err
	}

	return nil
}

// Return hostname based on reverse lookup DNS. In hostname remove last dot.
// If ip hast to record in dsn retrun "-"
// @param ip strin - ip addresse
// @retrun string - hostname or "-" if not found
func gethostname(ip string) string {

	var hostname string = "-"

	if ip != "" {

		// Reverse lockup DNS. ! LoockupAddr always return err != nil
		names, _ := net.LookupAddr(ip)

		// If result of lookup is not empty take first element and remove last dot.
		if len(names) > 0 {
			hostname = names[0]
			hostname = hostname[:len(hostname)-1]
		}
	}

	return hostname
}
