package myloger

// Create loger basend on standard packet "log" https://pkg.go.dev/log
// Log levels 5=Debug, 4=Info, 3=Warnung, 2=Error, 1=Fatal, 0=NoLog
// Create loger
// loger := NewMyloger(String "Path to Logfile", int Loglevel)

import (
	"log"
	"os"
)

type Loger struct {
	logfilepath string
	loglevel    int
	logtrace    *log.Logger
	logdebug    *log.Logger
	loginfo     *log.Logger
	logwarn     *log.Logger
	logerror    *log.Logger
	logfatal    *log.Logger
}

func (l *Loger) Trace(msg string) {
	if l.loglevel >= 6 {
		l.logdebug.Println(msg)
	}
}

func (l *Loger) Debug(msg string) {
	if l.loglevel >= 5 {
		l.logdebug.Println(msg)
	}
}

func (l *Loger) Info(msg string) {
	if l.loglevel >= 4 {
		l.loginfo.Println(msg)
	}
}

func (l *Loger) Warn(msg string) {
	if l.loglevel >= 3 {
		l.logwarn.Println(msg)
	}
}

func (l *Loger) Error(msg string) {
	if l.loglevel >= 2 {
		l.logerror.Println(msg)
	}
}

func (l *Loger) Fatal(msg string) {
	if l.loglevel >= 1 {
		l.logfatal.Println(msg)
		os.Exit(1)
	}
}

// Create new myloger return reference to struct myloger
// @param logfilepath string. If "" output logs to console
// @param loglevel int. 6=Trace,5=Devug, 4=Info, 3=Warnung, 2=Error, 1=Fatal, 0=NoLog
// @return pointer to logger
func New(logfilepath string, loglevel int) *Loger {

	l := new(Loger)
	l.logfilepath = logfilepath
	l.loglevel = loglevel

	// If defined path to logfile  all messages wrtite to this file, if not
	// output message sin console
	if logfilepath != "" {

		logfile, err := os.OpenFile(logfilepath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)

		if err != nil {
			log.Fatal(err)
		}

		l.logtrace = log.New(logfile, "TRACE|", log.Ldate|log.Ltime)
		l.logdebug = log.New(logfile, "DEBUG|", log.Ldate|log.Ltime)
		l.loginfo = log.New(logfile, "INFO |", log.Ldate|log.Ltime)
		l.logwarn = log.New(logfile, "WARN |", log.Ldate|log.Ltime)
		l.logerror = log.New(logfile, "ERROR|", log.Ldate|log.Ltime)
		l.logfatal = log.New(logfile, "FATAL|", log.Ldate|log.Ltime)

	} else {

		l.logtrace = log.New(os.Stdout, "TRACE|", log.Ldate|log.Ltime)
		l.logdebug = log.New(os.Stdout, "DEBUG|", log.Ldate|log.Ltime)
		l.loginfo = log.New(os.Stdout, "INFO |", log.Ldate|log.Ltime)
		l.logwarn = log.New(os.Stdout, "WARN |", log.Ldate|log.Ltime)
		l.logerror = log.New(os.Stdout, "ERROR|", log.Ldate|log.Ltime)
		l.logfatal = log.New(os.Stdout, "FATAL|", log.Ldate|log.Ltime)
	}

	return l
}

// Add apostrophes (Russsian kawichki) at begin and end of string
func Kawichki(msg string) string {
	return "\"" + msg + "\""
}
