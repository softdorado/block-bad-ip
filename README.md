# Block-Bad-IP

Block-Bad-IP is intelligent system to reduce possibility abuse online systeme.
* [Documenation English](https://gitlab.com/softdorado/block-bad-ip/-/tree/main/doc/block_bad_ip_en.md)
* [Documenation German](https://gitlab.com/softdorado/block-bad-ip/-/tree/main/doc/block_bad_ip_de.md)

[Download installationspackage](https://www.softdorado.com/block-bad-ip.html)

## Installation
```
unzip BBIP_*.zip  
./runinstall.sh
```
