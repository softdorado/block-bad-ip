#! /bin/bash

cd src
rm -f bbip 2>/dev/null
go build bbip.go

if [ $? -ne 0 ] ; then
    echo ERROR Failed buid binary bbip. Cancel build.
    exit 1
else
    echo Build binary ok.
fi 

## Get current version
./bbip -? | grep version

if [ $? -ne 0 ] ; then
    echo ERROR Version not found. Cancel build.
    exit 1
fi

VERSION=`./bbip -? | grep version | cut -d " " -f 3`
if [ -z "$VERSION" ] ; then
    echo ERROR Version is empty. Cancel build.
    exit 1
else
    echo Building package BBIP version $VERSION
fi

## Create directroy for new package
cd ..

if [ -d build ] ; then 
    rm -rf build

    if [ $? -ne 0 ] ; then
        echo ERROR Failed remove old build directory. Cancel build.
        exit 1
    fi
fi

mkdir -p build/bin
mkdir -p build/doc
mkdir -p build/man

cp src/bbip build/bin
cp src/bbipd build/bin
cp src/runinstall.sh build
cp src/rununinstall.sh build
cp -rf etc build

## Create HTML and Man-pages from MD

pandoc -v 1>/dev/null
if [ $? -ne 0 ] ; then 
    echo "ERROR Utility pandoc not found."
    exit 1
fi

## Create man pages
pandoc doc/block_bad_ip_en.md -s -t man -o build/man/bbip_en.1
if ! [ -f build/man/bbip_en.1 ] ; then
    echo ERROR Man-page build/man/bbip_en.1 not found. Cancel build.
    exit 1
fi
gzip build/man/bbip_en.1

pandoc doc/block_bad_ip_de.md -s -t man -o build/man/bbip_de.1
if ! [ -f build/man/bbip_de.1 ] ; then
    echo ERROR Man-page build/man/bbip_de.1 not found. Cancel build.
    exit 1
fi
gzip build/man/bbip_de.1

## Create HTML 
pandoc doc/block_bad_ip_en.md -s --metadata pagetitle="Block Bad IP" -o build/doc/bbip_en.html
pandoc doc/block_bad_ip_de.md -s --metadata pagetitle="Block Bad IP" -o build/doc/bbip_de.html

## Create packet as zip

cd build
zip -r BBIP_$VERSION.zip bin man etc doc *.sh

if [ $? -ne 0 ] ; then 
    echo Failed create ZIP. Cancel build.
    exit 1
else
    echo New packet BBIP_$VERSION.zip
fi

cd ..
