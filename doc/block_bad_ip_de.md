Block-Bad-IP Version 1.0.0.0 Copyright (c) 2021, Andrej Koslov. Distributed under BSD-3 License

---
# Block Bad IP - bbip

Cyberangriffe beginnen häufig mit der Suche nach Schwachstellen die sich aus der Ferne
ausnutzen lassen z.B. auf Webservern oder anderen online Services wie VPN,
Remote-Desktop, SSH etc. Dafür versuchen Scanner bekannte Schwachstellen oder
schwache Passwörter zu finden. Meistens hinterlassen diese Versuche Spuren in Logfiles.
Block-Bad-IP (bbip) erkennt diese Anomalien in Logfiles und blockiert die IP-Adressen der
Scanner. Eine andere Methode solche Scanner zu identifizieren ist Honeypot. bbip öffnet die
Ports von populären Online Service. Jede IP-Adresse, die versucht sich mit diesen Ports zu
verbinden wird geblockt, weil das die Scanner sind, die nach Schwachstellen suchen. Um
falsche Alarme zu vermeiden hat bbip Listen mit vertrauenswürdigen IP-Adressen und
FQDN z.B. für bekannte [Web-Crawler](https://en.wikipedia.org/wiki/Web_crawler). Außer der Erkennung eines Scanner, kann bbip auch die überdurchschnittliche Nutzung verhindern.

Für das Blockieren wird iptables `/usr/sbin/iptables` verwendet. Wenn eine IP-Adresse als
Scanner identifiziert wird(Bad-IP), wird sie zunächst für ein Stunde geblockt. Beim zweiten
Mal für einen Tag. Bei dritten Mal oder mehr für eine Woche. Wenn Bad-IP zwei Wochen nicht
wieder vorkommt, dann wird die IP aus der Datenbank entfernt.

## Struktur der Anwendung

`/usr/local/bin/bbip` - die ausführbare Datei. Die Startparameter:

* -c Pfad zu dem Konfigurationsordner. Default . oder /etc/bbip. In dem Ordner Programm erwartet config.xml, trusted-ip.txt, trusted-dns.txt und speichert Datenbank ipdb.csv.
* -l Pfad zu Logfile. Wenn nicht definiert Ausgabe auf Konsole.
* -v Loglevel 5 Debug, 4 Info (Default), 3 Warnung, 2 Error, 1 Fatal, 0 NoLog

`/usr/local/bin/bbipd` - Das Startscript. Die Startparameter: start|stop|status|restart

`/etc/bbip/setvar` - das Konfigurationsfile für Startscript. 

`/etc/bbip/trusted-dns.txt` - die Liste mit Regular Expressions (Muster) für vertrauenswürdige FQDN. Alle gefundenen Bad-IPs werden per DNS zu FQDN konvertiert und geprüft, ob diese in der Liste sind. z.B ”\.yandex.ru$” alle FQDN die mit “.yandex.ru” enden werden ignoriert.

`/etc/bbip/trusted-ip.txt` - die Liste mit Regular Expression (Muster) für vertrauenswürdige IP Adressen. Z.B. “^95\.223\.75\.”- alle IP Adressen die mit 95.223.75 anfangen, werden ignoriert.

## Konfigurationsfile

Beispiel für Erkennung eines Scanners auf Basis der Muster im Apache Logfile
```
<logfilerule>
    <name>Web scanner 1</name>
    <logpath>/var/log/httpd/access.log</logpath>
    <error>" [4,5]\d\d \d{1,3} ".*$</error>
    <exclude>robots.txt</exclude>
    <ip>(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) </ip>
    <timesmax>1</timesmax>
    <interval>2</interval>
    <enable>true</enable>
</logfilerule>
```
Beispiel für Honeypot.
```
<honeypotrule>
    <name>Microsoft RDP Server</name>
    <bind>127.0.0.1:3389</bind>
    <enable>false</enable>
</honeypotrule>
```
Weiter Beispiele finden Sie in /etc/bbip/config-example.xml

## Konfigurationsparameter

Die Konfigurationsparameter für Monitoring Logfile

* ``<logfilerule>`` - Regel für Monitoring Logfie
* ``<name>`` - Logischer Name für diese Regel
* ``<logpath>`` - Pfad zum Logfile
* ``<error>`` - Regular Expression für Erkennung eines Scanners im Logfile
* ``<exclude>`` - Regular Expression für Zeilen ignorieren. Z.b “robots\.txt”
* ``<ip>`` - Regular Expression für Erkennung einer Scanner IP Adresse. Die runden
Klammern für (Group) sind zwingend notwendig. Z.B
“(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})”.
* ``<timemax>`` - Wie oft soll Muster vorkommen um als Error interpretiert zu werden. Diese
Parameter kann man auch verwenden um Over-Usage zu erkennen.
* ``<interval>`` - In welchem Intervall prüfen Logfile. Wert in Sekunden.
* ``<enable>`` - Die Regel aktivieren = true oder deaktiveren = false.

Die Konfigurationsparameter für Monitoring Online-Service

* ``<honeypot>`` - Regel für Monitoring Online-Service bzw. Honeypot.
* ``<name>`` - Logischer Name für diese Regel
* ``<bind>`` - IP und Port wo dieser Service erreichbar wird
* ``<enable>`` - Die Regel aktivieren = true oder deaktiveren = false.