Block-Bad-IP Version 1.0.0.0 Copyright (c) 2021, Andrej Koslov. Distributed under BSD-3 License

---
# Block Bad IP - bbip


Cyber attacks often begin by looking for vulnerabilities that can be exploited remotely, e.g. in web servers or other online services such as VPN, remote desktop, SSH etc. Scanners try to find known vulnerabilities or weak passwords. Most of these attempts leave traces in log files. Block-Bad-IP (further bbip) recognizes these anomalies in log files and blocks IP
addresses of these scanners. Another method to identify scanners is honeypot. bbip opens
the ports of popular online services. Any IP address trying to connect to the port will be
blocked because these are the scanners looking for vulnerabilities. To avoid false alarms
bbip has lists with trusted IP addresses and FQDN e.g. for known [web crawlers](https://en.wikipedia.org/wiki/Web_crawler). Besides detecting a scanner, bbip can prevent over usage.

For blocking used iptables `/usr/sbin/iptables`. If an IP address is identified as a scanner
(further "Bad IP"), it will be blocked for an hour. Blocked for a day the second time. Blocked for a week on the third time or more. If Bad-IP does not occur again for two weeks, then this IP will be removed from the database.

## Structure of the application

`/usr/local/bin/bbip` - the executable. Start parameters:

* -c Path to the configuration folder. Default . or /etc/bbip. In the program folder expects
config.xml, trusted-ip.txt, trusted-dns.txt and stores database ipdb.csv.
* -l Path to log file. If not defined output to console.
* -v Loglevel 5 Debug, 4 Info (Default), 3 Warning, 2 Error, 1 Fatal, 0 NoLog

`/usr/local/bin/bbipd` - the start script. Start parameters: start|stop|status|restart

`/etc/bbip/setvar` - the configuration file for the start script.

`/etc/bbip/trusted-dns.txt` - the list of regular expressions (patterns) for trusted FQDN. All Bad-IP via DNS will be converted to FQDN and checked whether this one is in the list. E.g. ”\.yandex.ru$” all FQDN ending with “.yandex.ru” are ignored.

`/etc/bbip/trusted-ip.txt` - the regular expression list (pattern) for trusted IP addresses. E.g. “^95\.223\.75\.”- all IP addresses starting with 95.223.75, will be ignored.

## Configuration file

Sample scanner detection based on patterns in the Apache log file.
```
<logfilerule>
    <name>Web scanner 1</name>
    <logpath>/var/log/httpd/access.log</logpath>
    <error>" [4,5]\d\d \d{1,3} ".*$</error>
    <exclude>robots.txt</exclude>
    <ip>(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) </ip>
    <timesmax>1</timesmax>
    <interval>2</interval>
    <enable>true</enable>
</logfilerule>
```
Honeypot example.
```
<honeypotrule>
    <name>Microsoft RDP Server</name>
    <bind>127.0.0.1:3389</bind>
    <enable>false</enable>
</honeypotrule>
```
See /etc/bbip/config-example for more examples.

## Configuration parameters

Configurationsparameter for monitoring logfile

* `<logfilerule>` - rule for monitoring logfie
* `<name>` - Logical name for this rule
* `<logpath>` - path to log file
* `<error>` - Regular expression for detecting a scanner in the logfile
* `<exclude>` - Regular expression for rows that should be ignored. E.g. “robots\.txt”
* `<ip>` - Regular expression for detecting scanner IP address. The round brackets
for (group) are mandatory. E.g. “(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})”.
* `<timemax>` - How often should the pattern occur in order to be interpreted as an error.
These parameters can also be used to detect over usage.
* `<interval>` - At what interval to check the logfile. Value in seconds.
* `<enable>` - Enable the rule= true or disable= false.

Konfigurationsparameter for monitoring online-service

* `<honeypot>` - rule for monitoring online service or honeypot
* `<name>` - Logical name for this rule
* `<bind>` - IP and port where this service will be reachable
* `<enable>` - Enable the rule= true or disable= false.