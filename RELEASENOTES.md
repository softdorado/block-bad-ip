## Release notes

## Version 1.0.0.1
- In configuration example, rules "Web scanner..." exclude timeout (Status-Code 408), 
  because often occure by mobile devices.

## Version 1.0.0.0

First complet version. Package as ZIP-file with install and uninstall scripts.
Installer use examples in /etc/bbip only if configurations file not exists.

## Version 0.12.0.0 Beta (08.01.2022)
- In configuration for logmonitoring new XML element \<exclude\>. 
  The element contains reg. expression. Logmonitor ignore lines containing  
  excluded pattern.
- Rename element \<regexerror\> to \<error\>
- Rename element \<regexip\> to \<ip\>
- Trused ip addresses moved to separate file trusted-ip.txt Each line is reg. 
  expression.
- New trusted FQDN rusted-nds.txt Each line is reg. expression.
- Start-parameter -c now is configuraton directory. Programm expects
  in the directory config-file config.xml, files trusted-ip.txt,  
  trusted-dns.txt. Save there database ipdb.csv.
- In etc/bbip examples for all config. files.
- All functions for database moved to manager/ipdb.go
- Prevent more as one blocking rules for the same ip
